#include <stdio.h>
#include <stdlib.h>

// To accept command line input, we add two arguments to main. The first will contain the number of
// inputs on the command line, and the second is an array of C strings (pointers to characters) that
// contain what was written. Main is special and has to take no arguments, or these two arguments.
// (Other functions can take whatever arguments they like.)
int main(int argc, const char* argv[]) {
    // If the user didn't give us two numbers, we exit.
    if (argc < 3) {
        printf("Please type two numbers.\n");
        return 0;
    }

    // If the user gave us more than two numbers, we just ignore the rest.
    // Can you change the program so it adds all the numbers?
    if (argc > 3) {
        printf("Woah, there, that's a lot of numbers.\n");
    }

    int a = atoi(argv[1]);
    int b = atoi(argv[2]);
    int c = a + b;

    printf("%i + %i = %i\n", a, b, c);
}
