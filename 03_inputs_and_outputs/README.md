# Inputs and Outputs

So far we haven't interacted with our programs, except to read what they print to the terminal.
These examples illustrate how to make them accept command line arguments and read a file. Keep in
mind that this only applies on your computer -- I/O on a microcontroller will happen through its
peripherals, rather than the C standard library functions we use here.

```
demo@linux:03_inputs_and_outputs$ make
gcc -Wall -O3 -o add_integers add_integers.c
gcc -Wall -O3 -o read_file read_file.c

demo@linux:03_inputs_and_outputs$ ./add_integers 1 2
1 + 2 = 3

demo@linux:03_inputs_and_outputs$ ./read_file
This text is in a file.
```
