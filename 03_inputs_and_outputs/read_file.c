#include <stdio.h>

int main() {
    int c;
    FILE *file;

    // The second argument indicates that we just want to read the file. (We'd put "w" if we wanted
    // to write instead.)
    file = fopen("test.txt", "r");

    // The file might not have opened successfully. For instance if "test.txt" doesn't exist.
    if (file) {
        // We read characters from the file one at a time, and print them. There are more efficient
        // ways to load the data if you want to deal with bigger files, but this gets you pretty
        // far, especially compared to scripting languages.
        while ((c = getc(file)) != EOF) {
            // This is like printf, but for only one character.
            putchar(c);
        }
        fclose(file);
    }
}
