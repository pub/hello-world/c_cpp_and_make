# C, C++, and Makefiles

Microcontrollers are great and all, but if you're new to programming it can help to learn a bit
about the software tools on their own before diving into the hardware as well. This repo has some
example programs in C and C++, along with Makefiles that help you build them.

There are a lot of great tutorials out there, from this class and elsewhere.

- [last year's slides](https://gitlab.cba.mit.edu/classes/863.19/site/blob/master/doc/programming_rec/programming_rec_slides.pdf)

- [a few](https://www.tutorialspoint.com/cprogramming/index.htm)
  [of many](https://www.cprogramming.com/tutorial/c-tutorial.html)
  [online](https://beginnersbook.com/2014/01/c-tutorial-for-beginners-with-examples/)
  [tutorials](https://www.learn-c.org/)

This repo is meant to be more of a hands on reference. So clone it, build it, and play with it.

## The Basics

C is a programming language. It was created in the early 70s at Bell Labs by Dennis Ritchie and Ken
Thompson. This makes it quite old, as far as programming languages go, but it certainly wasn't the
first (Fortran, Lisp, COBOL, and BASIC are all older, to name a few). And it's still widely used
today -- number one, in fact, in some popularity [rankings](https://www.tiobe.com/tiobe-index/).

C++ is an extended version of C, created by Bjarne Stroustrup in the 80s. The syntax is basically
the same, so usually any C program you write will also be a valid C++ program. The original
difference is that C++ is [object
oriented](https://en.wikipedia.org/wiki/Object-oriented_programming) (basically, allows you to
associate data and methods with "classes"), but since then it's had all sorts of other stuff bolted
on too (like [templates](https://en.wikipedia.org/wiki/Template_(C%2B%2B)),
[lambdas](https://stackoverflow.com/questions/7627098/what-is-a-lambda-expression-in-c11), etc.). C
and C++ are commonly used together.

Unlike Python or JavaScript, C and C++ are compiled languages. This means that before you run your
program, you always convert it into a binary format that your computer can execute directly. This is
called building your program, and it is done by a compiler. To run code on your own computer, you'll
almost certainly end up using [gcc](https://gcc.gnu.org/), [clang](https://clang.llvm.org/), or
[MSVC](https://visualstudio.microsoft.com/vs/features/cplusplus/). The first two are usually run
from the command line, while the third is integrated into Visual Studio. We'll be using the UNIX
style command line exclusively, so even if you run Windows, it's recommended that you install a
command line toolchain.

## Required Software

Overall, we'll need a compiler, and [Make](https://www.gnu.org/software/make/).

On Linux, if these tools aren't installed already, you can use your package manager. Most distros
have one package that has all the basic tools bundled together. On Ubuntu, for example, you can run
`sudo apt install build-essential`.

On Mac, if you've installed XCode, you already have clang. (You may need to tell XCode to install
the command line tools specifically for it to show up on your path.) You can also use homebrew to
install gcc. One gotcha is that Apple decided to alias clang as gcc in the stock install. So if you
run gcc, you'll actually get clang. This is bad because not all the options are the same, so
sometimes it just won't work to switch between the two. So if you do install gcc with homebrew,
you'll want to run gcc-8 or gcc-9 (or whatever specific version you got).

On Windows... things are trickier. Most Windows applications assume you'll always use the graphical
user interface (GUI) and not the command line. You could install the [Windows Subsystem for
Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10). This will work great for
compiling C to run on your own computer, but it might lead you into trouble when programming
microcontrollers (people often complain of USB errors in WSL). Before WSL,
[MinGW](http://www.mingw.org/) was the go-to command line environment. Overall you'll probably have
a smoother experience if you bite the bullet and find/install Linux up front.

## Examples

- [hello world](./01_hello_world)
- [fundamentals](./02_fundamentals)
- [inputs and outputs (on your computer)](./03_inputs_and_outputs)
- [bit twiddling](./04_bit_twiddling)
