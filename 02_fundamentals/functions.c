#include <stdio.h>

int is_even(int x) {
    return x % 2 == 0;
}

int is_odd(int x) {
    return x % 2 == 1;
}

int is_prime(int x) {
    for (int i = 2; i <= x / 2; ++i) {
        if (x % i == 0) {
            return 0;
        }
    }
    return 1;
}

int factorial(int x) {
    int result = 1;
    for (int i = 2; i <= x; ++i) {
        result *= i;
    }
    return result;
}

int n_choose_k(int n, int k) {
    // Note: if you actually want to compute binomial coefficients, there are better ways to do it.
    // This method will overflow except for very small n and k.
    return factorial(n) / (factorial(k) * factorial(n - k));
}

int main(void) {
    int two_is_even = is_even(2);
    int three_is_odd = is_odd(3);
    int four_is_prime = is_prime(4);
    int five_factorial = factorial(5);
    int six_choose_four = n_choose_k(6, 4);

    printf(two_is_even ? "two is even\n" : "two is odd\n");
    printf(three_is_odd ? "three is odd\n" : "three is even\n");
    printf(four_is_prime ? "four is prime\n" : "four isn't prime\n");
    printf("5! = %i\n", five_factorial);
    printf("6 choose 4 = %i\n", six_choose_four);
}
