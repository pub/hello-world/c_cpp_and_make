#include <stdio.h>

int main(void) {
    // Here we declare an integer called i, and set it equal to zero.
    int i = 0;

    // This is a while loop. The code in the braces will run repeatedly as long as the condition is
    // true. You don't actually need braces if there's only one statement (i.e. line). But it's not
    // a bad habit to write them anyway, so that you don't accidentally skip anything. Unlike in
    // Python, the indentation doesn't mean anything to the compiler -- it's just for you.
    while (i < 5) {
        printf("while loop: %i\n", i);
        // This increments i. We could also use "i = i + 1" or "i += 1".
        ++i;
    }

    // This is a for loop. It's like a while loop, but you name the iteration variable, state the
    // loop condition, and write your increment statement (or whatever else you want to do at the
    // end of each iteration) all up front.
    for (int j = 0; j < 5; ++j) {
        printf("for loop: %i\n", j);
    }

    // In C, 0 means false, but any other integer means true (usually we use 1). So this loop would
    // run forever, except we we have a trick up our sleeve: you can exit the loop with the keyword
    // "break". This works in for loops too.
    i = 0;
    while (1) {
        printf("another while loop: %i\n", i);

        // This one is pretty obvious, but it's arguably the most important control structure.
        if (i == 2) {
            printf("leaving the while loop\n");
            break;
        }

        ++i;
    }

    // Another useful loop keyword is "continue". It jumps to the next loop iteration early. Like
    // "break", it works in for loops and while loops. Also note that we can reuse i here, like
    // we've been doing in our while loops. Both loops work both ways, but it's more common to see
    // for loops declare new variables.
    for (i = 0; i < 5; ++i) {
        // Here we use the modulo operator "%" to check if i is even.
        if (i % 2 == 0) {
            continue;
        }

        // This code only runs if i is odd.
        printf("%i is odd\n", i);
    }

    // This is a switch statement. It's not a loop, but it is handy to dispatch to different pieces
    // of code based on the value of some variable.
    i = 42;
    switch (i) {
        case 0:
            printf("i is 0\n");
            break;
        case 1:
            printf("i is 1\n");
            break;
        case 2:
            printf("i is 2\n");
            break;
        case 42:
            printf("i is 42\n");
            break;
        default:
            printf("i is something else\n");
    }
}
