# Fundamentals

These examples demonstrate C's basic types, how to use control structures like branches and loops,
and how to write functions. These things can get you pretty far. Some more advanced topics that
aren't (yet) covered here are pointers, and the difference between values and references. These
would be good next steps if you want to gain a deeper understanding of the language.

```
demo@linux:02_fundamentals$ make
gcc -Wall -O3 -o basic_types basic_types.c
gcc -Wall -O3 -o control_structures control_structures.c
gcc -Wall -O3 -o functions functions.c
```

```
demo@linux:02_fundamentals$ ./basic_types
Signed Integers
min: -2147483648
max: 2147483647
5 - 10 = -5

Unsigned Integers
min: 0
max: 4294967295
5 - 10 = 4294967291

Floating Point
min: -3.402823e+38
smallest positive (normalized): 1.175494e-38
max: 3.402823e+38
closest float to 1.2: 1.2000000477
1.2 - 0.2: 1.0000000477
(0.001 + 1.0) - 1.0 = 0.001000047
0.001 + (1.0 - 1.0) = 0.001000000

Double Precision Floating Point
min: -1.797693e+308
smallest positive (normalized): 2.225074e-308
max: 1.797693e+308
closest double to 1.2: 1.19999999999999996
1.2 - 0.2: 1.00000000000000000
(1e-6 + 1.0) - 1.0 = 0.000001000000000000024
1e-6 + (1.0 - 1.0) = 0.000001000000000000000

Character
1$
numeric 0 (won't show up as anything):
ASCII 0: 0

Strings
hello
```

```
demo@linux:02_fundamentals$ ./control_structures
while loop: 0
while loop: 1
while loop: 2
while loop: 3
while loop: 4
for loop: 0
for loop: 1
for loop: 2
for loop: 3
for loop: 4
another while loop: 0
another while loop: 1
another while loop: 2
leaving the while loop
1 is odd
3 is odd
i is 42
```

```
demo@linux:02_control_structures$ ./functions
two is even
three is odd
four isn't prime
5! = 120
6 choose 4 = 15
```
