#include <iostream>

// C++ uses a main routine just like C. We could use printf here, too, but it's more idiomatic to
// use std::cout.
int main() {
    std::cout << "hello, world, I'm C++\n";
}
