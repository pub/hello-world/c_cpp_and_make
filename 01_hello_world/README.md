# Hello World

From this directory (i.e. first run `cd /wherever/you/put/this/repo/01_hello_world`), run `make`. If
your toolchain is installed correctly, this will build two programs: `hello_world_c` and
`hello_world_cpp`. You can run them like this: `./hello_world_c` and `./hello_world_cpp`.

```
demo@linux:01_hello_world$ make
gcc -Wall -O3 -o hello_world_c hello_world.c
g++ -Wall -O3 -o hello_world_cpp hello_world.cpp

demo@linux:01_hello_world$ ./hello_world_c
hello, world, I'm C

demo@linux:01_hello_world$ ./hello_world_cpp
hello, world, I'm C++
```

Note: Putting `./` in front of the executable files tells the shell unambiguously that you want to
run the programs located in this directory. The shell will say something like `command not found` if
you leave it out. This might seem annoying, but it's really important from a security perspective.
Otherwise if you made a program called `cd` or `make` or something, you could accidentally run it
and it might do something bad.
